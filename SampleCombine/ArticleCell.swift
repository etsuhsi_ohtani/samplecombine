//
//  ArticleCell.swift
//  SampleCombine
//
//  Created by Etsushi Otani on 2021/08/28.
//

import Foundation
import UIKit
import Combine

class ArticleCell: UITableViewCell {
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleLabel: UILabel!
    private var cancellable: AnyCancellable?
    static let nibname = "ArticleCell"
    
    func loadImage(urlStr: String) {
        cancellable = ImageLoader().load(urlStr: urlStr).receive(on: RunLoop.main).sink(receiveCompletion: { error in
            print(error)
        }, receiveValue: { data in
            self.articleImageView.image = UIImage(data: data)
        })
    }
}
