//
//  ImageLoader.swift
//  SampleCombine
//
//  Created by Etsushi Otani on 2021/08/29.
//

import Foundation
import Combine

struct ImageLoader {
    
    func load(urlStr: String) ->Future<Data, Error>{
        return Future<Data, Error> { result in
            guard let url = URL(string: urlStr),
                  let data = try? Data(contentsOf: url) else {
                result(.failure(URLError(.badURL)))
                return
            }
            result(.success(data))
        }
    }
}
