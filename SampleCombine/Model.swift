//
//  ArticleModel.swift
//  SampleCombine
//
//  Created by Etsushi Otani on 2021/08/28.
//

import Foundation

struct Article: Codable {
    var id: String
    var createdAt: Date
    var title: String
    var url: String
    var user: User
    
    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created_at"
        case title
        case url
        case user
    }
}

extension Article {
    init(from decoder: Decoder) throws {
        let value = try decoder.container(keyedBy: CodingKeys.self)
        id = try value.decode(String.self, forKey: .id)
        createdAt = try value.decode(Date.self, forKey: .createdAt)
        title = try value.decode(String.self, forKey: .title)
        url = try value.decode(String.self, forKey: .url)
        user = try value.decode(User.self, forKey: .user)
    }
}

struct User: Codable {
    var id: String
    var name: String
    var profileImageURL: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case profileImageURL = "profile_image_url"
    }
}

extension User {
    init(from decoder: Decoder) throws {
        let value = try decoder.container(keyedBy: CodingKeys.self)
        id = try value.decode(String.self, forKey: .id)
        name = try value.decode(String.self, forKey: .name)
        profileImageURL = try value.decode(String.self, forKey: .profileImageURL)
    }
}
