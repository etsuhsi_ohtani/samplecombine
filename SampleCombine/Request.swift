//
//  Request.swift
//  SampleCombine
//
//  Created by Etsushi Otani on 2021/08/28.
//

import Foundation
import Combine

class APIEndPoint {
    static let baseURL = "https://qiita.com/api/v2/"
}

struct ArticleRequest {
    private let path = "/items"
    private var url: String {
        return APIEndPoint.baseURL + path
    }
    private let method = "GET"
    private var header = ["contentType": "application/json"]
    private let perPage = 50
    
    func exec(page: Int, searchText: String) ->AnyPublisher<[Article], Error>{
        var components = URLComponents(string: url)!
        components.queryItems =
            [URLQueryItem(name: "page", value: String(page)),
             URLQueryItem(name: "per_page", value: String(perPage))]
        var request = URLRequest(url: components.url!)
        request.allHTTPHeaderFields = header
        request.httpMethod = method
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let result = URLSession.shared.dataTaskPublisher(for: request)          // Publisher作成
            .tryMap { element -> Data in                                        // Operatorで加工
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                return element.data
            }
            .decode(type: [Article].self, decoder: decoder)
            .eraseToAnyPublisher()                                              // AnyPublisher型へ変更
        return result
    }
}

