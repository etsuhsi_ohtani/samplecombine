//
//  ViewController.swift
//  SampleCombine
//
//  Created by Etsushi Otani on 2021/08/28.
//

import UIKit
import Combine

class ViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel = ViewModel()
    private var cancellable: AnyCancellable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: ArticleCell.nibname, bundle: nil), forCellReuseIdentifier: ArticleCell.nibname)
        cancellable = viewModel.articleSubject.receive(on: RunLoop.main).sink(receiveValue: { _ in
            self.tableView.reloadData()                 // ViewModelの値の更新を受けてTableViewをリロード
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.fetchAricle()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.articleSubject.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.nibname) as! ArticleCell
        let article = viewModel.articleSubject.value[indexPath.row]
        cell.loadImage(urlStr: article.user.profileImageURL)
        cell.articleLabel.text = article.title
        print(article)
        return cell
    }
    
    
}
