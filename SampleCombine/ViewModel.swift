//
//  ViewModel.swift
//  SampleCombine
//
//  Created by Etsushi Otani on 2021/08/28.
//

import Foundation
import Combine
import UIKit

class ViewModel {
    let articleSubject: CurrentValueSubject<[Article], Never> = .init([])
    
    private var cancellable: AnyCancellable?
    private var page = 1
    private var searchText = ""
    
    func fetchAricle() {
        if cancellable != nil {
            cancellable?.cancel()
        }
        cancellable = ArticleRequest()
            .exec(page: page, searchText: searchText)
            .sink(receiveCompletion: { error in                     // Subscriberでイベント購読
                print(error)
            }, receiveValue: { articles in
                self.articleSubject.send(articles)                  // Subjectに値を投げる
                print(articles)                                     // Subjectは外から値を受け取ると
            })                                                      // イベントを発行するPublisher
    }
}

